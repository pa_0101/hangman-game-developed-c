/****************************************************************************
* COSC2138/CPT 220 - Programming Principles 2A
* Study Period 2  2015 Assignment #1 - hangman program
* Full Name        : Paolo Felicelli
* Student Number   : 3427174
* Course Code      : CPT220
* Start up code provided by the CTeach Team
****************************************************************************/


/* Header files. */
#include <stdio.h>
#include <stdlib.h>

/* My header inclusions */
#include <math.h> /* math.h has been included to
                     rand() function */
#include <time.h> /* time.h has been included to
                     provide the time() function */
#include <string.h> /* string.h has been included to provide the 
                       strcpy() and strlen functions */
#include <ctype.h> /* ctype.h has been included to 
                      provide the isalpha() function */


/* Constants. */
#define NUM_WORDS 50
#define ALPHABET_SIZE 26
#define GOOD_GUESS 0
#define BAD_GUESS 1
#define GAME_OVER 1
#define GAME_CONTINUE 0
#define MAX_GUESS 10
#define MAX_WORD_LEN 10

/* My constants */
#define TRUE 1  
#define FALSE 0 
#define SUCCESS 1
#define FAILURE 0 /* TRUE/FALSE/SUCCESS/FAILURE constants 
                     defined for using as general purpose boolean flags */

#define TEMP_STRING_LENGTH 1 
#define SINGLE_CHAR_INPUT 1 /* Constants used for char array sizes */

#define LOWERCASE_A 97 /* 97 corresponds to lower case 'a' in the ascii
                          table */

#define NEW_LINE printf("\n") /* Prints a new line for formating text to 
                                             screen */

/* Function prototypes. */
void init(char* word);
void displayWord(char* word, int* guessedLetters);
int guessLetter(char* word, int* guessedLetters);
void displayHangman(unsigned wrongGuesses);
int isGameOver(char* word, int* guessedLetters, unsigned wrongGuesses);
void readRestOfLine();

/* My prototypes */
int getChar(char *string, unsigned length);
void findMatch(int *guessedLetters, char *word, char *displayWord);
void showWord(char *displayWord, char *word);
void printPlusMinusChars(int *len);




/****************************************************************************
* COSC2138/CPT 220 - Programming Principles 2A
* Study Period 2  2015 Assignment #1 - hangman program
* Full Name        : Paolo Felicelli
* Student Number   : 3427174
* Course Code      : CPT220
* Start up code provided by the CTeach Team
****************************************************************************/

#include "hangman.h"

/* Static rightGuesses maintains a count of the right guesses */
static unsigned rightGuesses = 0;

/****************************************************************************
* Function main() is the entry point for the program.
****************************************************************************/
int main(void)
{
   char word[MAX_WORD_LEN + 1];
   unsigned wrongGuesses = 0;
   int guessedLetters[ALPHABET_SIZE] = {
      0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

   int guess;

   /* Choose a random word from words array and copy it into word array */
   init(word);

   /* Enter the game loop */
   do
   {
      /* Display the word with correctly guessed chars or underscores */
      displayWord(word, guessedLetters);

      /* Call guessLetter(), return GOOD or BAD guess and update the 
      wrongGuesses variable */
      guess = guessLetter(word, guessedLetters);

      if(guess == BAD_GUESS)
      {
         wrongGuesses++;
      }

      /* Call displayHangman() with the updated wrongGuesses variable as 
         parameter */
      displayHangman(wrongGuesses);

   /* do while continues until == GAME_OVER */
   } while(isGameOver(word, guessedLetters, wrongGuesses) != GAME_OVER);
   
   return EXIT_SUCCESS;
}


/****************************************************************************
* Function init() choses a word for the hangman game from the words[] array.
****************************************************************************/
void init(char* word)
{
   const char* words[NUM_WORDS] = {
      "array",      "auto",       "break",      "case",       "cast",
      "character",  "comment",    "compiler",   "constant",   "continue",
      "default",    "double",     "dynamic",    "else",       "enum",
      "expression", "extern",     "file",       "float",      "function",
      "goto",       "heap",       "identifier", "library",    "linker",
      "long",       "macro",      "operand",    "operator",   "pointer",
      "prototype",  "recursion",  "register",   "return",     "short",
      "signed",     "sizeof",     "stack",      "statement",  "static",
      "string",     "struct",     "switch",     "typedef",    "union",
      "unsigned",   "variable",   "void",       "volatile",   "while"
   };

   int randElement; 
   
   /* Choose a word from a random element in the char array, 
      store into randElement */
   srand(time(0));
   randElement = rand() % 50;

   /* Dereference at the random location and copy into the word char array */
   strcpy(word, *(words + randElement));
}


/****************************************************************************
* Function displayWord() prints the word to screen with all letters that 
* have not been correctly guessed blanked out with an underscore. 
* Output example:
* +-+-+-+-+-+-+-+-+-+-+
* |i|d|e|n|_|i|_|i|e|r|
* +-+-+-+-+-+-+-+-+-+-+
****************************************************************************/
void displayWord(char* word, int* guessedLetters)
{
   int i, len;
   char displayWord[MAX_WORD_LEN + 1];
   
   /* Get the length of the word */
   len = strlen(word);

   /* Loop through displayWord and init to 0 */
   for(i = 0; i < len; i++)
   {
      displayWord[i] = 0;
   }

   NEW_LINE;

   /* Print the top row of +- characters */
   printPlusMinusChars(&len);
   
   /* Find a match between the guessed letter and the corresponding 
      letter in the word */
   findMatch(guessedLetters, word, displayWord);

   /* Display the hidden and updated versions of the hidden word */
   showWord(displayWord, word);

   /* Print the bottom row of +- characters */
   printPlusMinusChars(&len);
   NEW_LINE;
}


/****************************************************************************
* Function guessLetter() prompts the user to enter one letter. The function
* maintains an array of guessed letters. The function returns GOOD_GUESS
* or BAD_GUESS depending on whether or not the letter is in the word.
****************************************************************************/
int guessLetter(char* word, int* guessedLetters)
{
   char ch[SINGLE_CHAR_INPUT + 1];
   char guessedLetter;
   char a = 'a';
   int c, i, len, alphaElement;
   int isFound = FALSE;
   int k = LOWERCASE_A;

   /* Get the length of the word */
   len = strlen(word);

   /* Get a char from the user and return it */
   getChar(ch, SINGLE_CHAR_INPUT);
   guessedLetter = *ch; 

   /* Loop through the word and check if it contains the char */
   for(i = 0; i < len; i++)
   {
      c = word[i];

      if(c == guessedLetter)
      {
         /* ASCII arithmatic to find array element that corresponds to
           the number of the letter in the ascii table */
         alphaElement = guessedLetter - a;
         guessedLetters[alphaElement] = (alphaElement + k);

         /* If the word contains the guessed letter, flip isFound to true */
         isFound = TRUE;

         /* If the word contains the guessed letter, add 1 to the 
            rightGuesses count */
         rightGuesses++;
      }
   }

   if(isFound == FALSE)
   {
      alphaElement = guessedLetter - a;
      guessedLetters[alphaElement] = (alphaElement + k);
      return BAD_GUESS;
   }
    
   else 
      return GOOD_GUESS;
}


/****************************************************************************
* Function displayHangman() displays an ascii art drawing to complement the
* game. The drawing varies depending on the number of wrong guesses.
* When there are no wrong guesses, an empty drawing is displayed:
* **********
* *        *
* *        *
* *        *
* *        *
* *        *
* *        *
* **********
* When there are 10 wrong guesses (and the game is over), the complete
* drawing is displayed:
* **********
* * +--+   *
* * |  |   *
* * |  O   *
* * | -+-  *
* * | / \  *
* * +----- *
* **********
* You need to display an appropriate drawing depending on the number of 
* wrong guesses:
* - 0 wrong: empty drawing.
* - 1 wrong: include floor.
* - 2 wrong: include vertical beam.
* - 3 wrong: include horizontal beam.
* - 4 wrong: include noose.
* - 5 wrong: include head.
* - 6 wrong: include body.
* - 7 wrong: include left arm.
* - 8 wrong: include right arm.
* - 9 wrong: include left leg.
* - 10 wrong: include right leg (complete drawing).
****************************************************************************/
void displayHangman(unsigned wrongGuesses)
{
   /* The working of this function is self explanitory */
   if(wrongGuesses == 0)
   {
      printf("\n\n");
      printf("**********\n");
      printf("*        *\n");
      printf("*        *\n");
      printf("*        *\n");
      printf("*        *\n");
      printf("*        *\n");
      printf("*        *\n");
      printf("**********\n\n");
   }

   if(wrongGuesses == 1)
   {
      printf("\n\n");
      printf("**********\n");
      printf("*        *\n");
      printf("*        *\n");
      printf("*        *\n");
      printf("*        *\n");
      printf("*        *\n");
      printf("* +----- *\n");
      printf("**********\n\n");
   }

   if(wrongGuesses == 2)
   {
      printf("\n\n");
      printf("**********\n");
      printf("*        *\n");
      printf("* |      *\n");
      printf("* |      *\n");
      printf("* |      *\n");
      printf("* |      *\n");
      printf("* +----- *\n");
      printf("**********\n\n");
   }

   if(wrongGuesses == 3)
   {
      printf("\n\n");
      printf("**********\n");
      printf("* +--+   *\n");
      printf("* |      *\n");
      printf("* |      *\n");
      printf("* |      *\n");
      printf("* |      *\n");
      printf("* +----- *\n");
      printf("**********\n\n");
   }

   if(wrongGuesses == 4)
   {
      printf("\n\n");
      printf("**********\n");
      printf("* +--+   *\n");
      printf("* |  |   *\n");
      printf("* |      *\n");
      printf("* |      *\n");
      printf("* |      *\n");
      printf("* +----- *\n");
      printf("**********\n\n");
   }

   if(wrongGuesses == 5)
   {
      printf("\n\n");
      printf("**********\n");
      printf("* +--+   *\n");
      printf("* |  |   *\n");
      printf("* |  O   *\n");
      printf("* |      *\n");
      printf("* |      *\n");
      printf("* +----- *\n");
      printf("**********\n\n");
   }

   if(wrongGuesses == 6)
   {
      printf("\n\n");
      printf("**********\n");
      printf("* +--+   *\n");
      printf("* |  |   *\n");
      printf("* |  O   *\n");
      printf("* |  +   *\n");
      printf("* |      *\n");
      printf("* +----- *\n");
      printf("**********\n\n");
   }

   if(wrongGuesses == 7)
   {
      printf("\n\n");
      printf("**********\n");
      printf("* +--+   *\n");
      printf("* |  |   *\n");
      printf("* |  O   *\n");
      printf("* | -+   *\n");
      printf("* |      *\n");
      printf("* +----- *\n");
      printf("**********\n\n");
   }

   if(wrongGuesses == 8)
   {
      printf("\n\n");
      printf("**********\n");
      printf("* +--+   *\n");
      printf("* |  |   *\n");
      printf("* |  O   *\n");
      printf("* | -+-  *\n");
      printf("* |      *\n");
      printf("* +----- *\n");
      printf("**********\n\n");
   }

   if(wrongGuesses == 9)
   {
      printf("\n\n");
      printf("**********\n");
      printf("* +--+   *\n");
      printf("* |  |   *\n");
      printf("* |  O   *\n");
      printf("* | -+-  *\n");
      printf("* | /    *\n");
      printf("* +----- *\n");
      printf("**********\n\n");
   }

   if(wrongGuesses == 10)
   {
      printf("\n\n");
      printf("**********\n");
      printf("* +--+   *\n");
      printf("* |  |   *\n");
      printf("* |  O   *\n");
      printf("* | -+-  *\n");
      printf("* | / \\  *\n");
      printf("* +----- *\n");
      printf("**********\n\n");
   }
}


/****************************************************************************
* Function isGameOver() is the final step in the program. The game is over
* if either all letters in the word have been guessed, or the player has run
* out of guesses. The player is congratulated if he/she wins. The word is
* displayed to the player if he/she loses. This function returns either 
* GAME_OVER or GAME_CONTINUE.
****************************************************************************/
int isGameOver(char* word, int* guessedLetters, unsigned wrongGuesses)
{
   int len;

   /* Get the word length */
   len = strlen(word);
   
   /* If the users wrong guesses meets tha amount of max guesses, 
      then it's game over */
   if(wrongGuesses == MAX_GUESS)
   {
      printf("Game over, the hidden word was: %s\n\n", word);
      return GAME_OVER;
   }

   /* If the users right guesses matches the length of the word, then user 
      wins the game */
   if(rightGuesses == len)
   {
      displayWord(word, guessedLetters);
      printf("Congratulations you won!\n\n");
      return GAME_OVER;
   }

   else
      return GAME_CONTINUE;
}


/****************************************************************************
* Function readRestOfLine() is used for buffer clearing. Source: 
* https://inside.cs.rmit.edu.au/~sdb/teaching/C-Prog/CourseDocuments/
* FrequentlyAskedQuestions/
****************************************************************************/
void readRestOfLine()
{
   int c;

   /* Read until the end of the line or end-of-file. */   
   while ((c = fgetc(stdin)) != '\n' && c != EOF)
      ;

   /* Clear the error and end-of-file flags. */
   clearerr(stdin);
}


/****************************************************************************
* getChar() function prompts the user to enter a single character. Also 
* performs validation for input length and character matching. 
* Note: This function is a variation of the getInteger() function provided 
* by RMIT. It has been modified to be made of use for the guessLetter() 
* function.
****************************************************************************/
int getChar(char *string, unsigned length)
{
   int isValid = FALSE;
   char tempString[TEMP_STRING_LENGTH + 2];

   /* Loop mechanism that requests user for correct input */
   while(isValid == FALSE)
   {
      /* Provide prompt */
      printf("Enter a letter (a - z): ");
      
      /* Accept input. "+2" is for the \n and \0 characters */
      fgets(tempString, length + 2, stdin);

      /* A string that doesn't have a newline character is too long. */
      if(tempString[strlen(tempString) - 1] != '\n')
      {
         printf("Input was too long.\n");
         readRestOfLine();
      }

      /* Check that user enters correct character */
      if(isalpha(*tempString) == FALSE || *(tempString + 1) != '\n') 
      {
         printf("Input must be a letter (a - z)\n\n");
      }

      else
      {
         /* Flip isValid to true after correct input to break out of loop */
         isValid = TRUE;
      }
   } 

   /* Overwrite the \n character with \0. */
   tempString[strlen(tempString) - 1] = '\0';
   
   /* Make the result string available to calling function. */
   strcpy(string, tempString);

   return SUCCESS;
}


/****************************************************************************
* This function finds a match between the guessed letter and the 
* corresponding letter in the hidden word.
****************************************************************************/
void findMatch(int *guessedLetters, char *word, char *displayWord)
{
   int i, j, len;
   char gsdLetter, wordLetter;

   /* Get the word length */
   len = strlen(word);

   /* Loop through each slot of the guessed letters array */
   for(i = 0; i < ALPHABET_SIZE; i++)
   {
      gsdLetter = guessedLetters[i];
      
      /* Loop through each slot of the word array */
      for(j = 0; j < len; j++)
      {
         wordLetter = word[j];

         /* Check if a guessed letter matches the corresponding letter in 
            the word */
         if(wordLetter == gsdLetter)
         {  
            displayWord[j] = gsdLetter;
         }
      }
   }
}


/****************************************************************************
* This function displays the hidden and updated versions of the hidden 
* word.
****************************************************************************/
void showWord(char *displayWord, char *word)
{
   int i, len;

   /* Get the length of the word */
   len = strlen(word);
    
   /* Loop through the length of the word */ 
   for(i = 0; i < len; i++)
   {  
      /* If corresponding array slot is 0(empty) print underscore */
      if(displayWord[i] == 0)
      {
         printf("|_");
      }

      /* Else print the correctly guessed letter */
      else
         printf("|%c", displayWord[i]);  
   }
   printf("|\n");
}


/****************************************************************************
* This function prints the top and bottom rows of +- characters with the 
* hidden word.
****************************************************************************/
void printPlusMinusChars(int *len)
{
   int i;
   int wordLen;

   wordLen = *len; 

   for(i = 0; i < wordLen; i++)
   {
      printf("+-");
   }

   printf("+\n");
}


